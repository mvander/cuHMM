/*
hmm.h

Declare HMM class.
 */

#ifndef HMM_H
#define HMM_H

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <boost/numeric/ublas/matrix.hpp>

class hmm {

 private:
  
  // Initial state distribution.
  std::vector<float> pi;
  // Transition distribution.
  boost::numeric::ublas::matrix<float> A;
  // Emission distribution.
  // Should be M rows, V columns.
  boost::numeric::ublas::matrix<float> B;

  // Number of hidden states.
  int M;
  // Vocabulary size.
  int V;

 public:

  hmm();
  ~hmm();

  /**
   * Setup our model using the pi, A, and B defined in the provided file.
   *
   * Format of file should be:
   * 
   * M,V
   * <comma-separated pi values>
   * <comma-separated A values>
   * <comma-separated B values>
   *
   * Where M is num. of states and V is num. of discrete values.
   * pi should be M long, A should be MxM long, B should be MxV long.
   * NO SPACES.
   *
   * @param filename - file containing pi,A,B definitions.
   */
  void set_model(std::string filename);

  /**
   * Forward Algorithm:
   * Determine the likelihood of each sequence in X using the forward algorithm.
   * 
   * @param X - Array of sequences.
   * @param x_length - Length of each sequence (allow to differentiate within X).
   * @return Probabilities of each sequence in X.
   */
  std::vector<float> forward(std::vector<int> X, int x_length);
  
};

#endif
