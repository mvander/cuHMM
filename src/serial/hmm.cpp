/*
hmm.cpp

Serial implementation of the forward algorithm.
 */

#include "hmm.h"
#include <math.h>
#include <boost/algorithm/string.hpp>

hmm::hmm() {
  // Nothing for now.
}

hmm::~hmm() {
  // Nothing for now.
}

void hmm::set_model(std::string filename) {
  std::ifstream setup_file;
  setup_file.open(filename);

  std::vector<std::string> m_v;
  std::string line;
  if(!std::getline(setup_file, line))
    return;
  boost::split(m_v, line, boost::is_any_of(","));

  M = std::stoi(m_v[0]);
  V = std::stoi(m_v[1]);

  // Resize collections.
  pi.resize(M);
  A.resize(M, M);
  B.resize(M, V);

  // TODO: Add error checking on sizes.

  // Read in pi.
  std::vector<std::string> pi_str;

  if(!std::getline(setup_file, line))
    return;
  boost::split(pi_str, line, boost::is_any_of(","));

  for(int idx = 0; idx < M; ++idx) {
    pi[idx] = std::stof(pi_str[idx]);
  }

  // Read in A.
  std::vector<std::string> A_str;
  if(!std::getline(setup_file, line))
    return;
  boost::split(A_str, line, boost::is_any_of(","));

  for (int idy = 0; idy < M; ++idy) {
    for (int idx = 0; idx < M; ++idx) {
      A(idy,idx) = std::stof(A_str[idy * M + idx]);
    }
  }

  // Read in B.
  std::vector<std::string> B_str;
  if(!std::getline(setup_file, line))
    return;
  boost::split(B_str, line, boost::is_any_of(","));

  for (int idy = 0; idy < M; ++idy) {
    for (int idx = 0; idx < V; ++idx) {
      B(idy,idx) = std::stof(B_str[idy * V + idx]);
    }
  }

  // Print out values:

  std::cout << "Pi:" << std::endl;
  
  for(int idx = 0; idx < M; ++idx) {
    std::cout << pi[idx] << ",";
  }

  std::cout << std::endl << "A:" << std::endl;

  for (int idy = 0; idy < M; ++idy) {
    for (int idx = 0; idx < M; ++idx) {
      std::cout << A(idy, idx) << ",";
    }
    std::cout << std::endl;
  }

  std::cout << "B:" << std::endl;

  for (int idy = 0; idy < M; ++idy) {
    for (int idx = 0; idx < V; ++idx) {
      std::cout << B(idy, idx) << ",";
    }
    std::cout << std::endl;
  }

}

std::vector<float> hmm::forward(std::vector<int> X, int x_length) {
  int num_of_seqs = X.size() / x_length;

  if (X.size() % x_length != 0) {
    std::cout << "Bad data provided." << std::endl;
    std::vector<float> empty;
    return empty;
  }

  // Store all probabilities in a vector.
  std::vector<float> probabilities;

  // For each sequence.
  for (int seq_num = 0; seq_num < num_of_seqs; ++seq_num) {
    // Create alpha to hold forward values.
    boost::numeric::ublas::matrix<float> alpha(M, x_length);
    // Create vector to hold our scaled values.
    std::vector<float> scales;

    // Start distribution determination.
    float scale = 0.0;
    
    // Calculate alpha prime and find c.
    int first_obs = X[seq_num*x_length];
    for (int state = 0; state < M; ++state) {
      float alph = pi[state] * B(state, first_obs);
      alpha(state, 0) = alph;
      scale += alph;
    }
    scales.push_back(scale);

    // Divide all start distributions by the starting scale.
    for (int state = 0; state < M; ++state) {
      alpha(state, 0) = alpha(state, 0) / scales[0];
    }

    // Now use transition matrix to find probability of each potential transition.
    for (int obs_idx = 1; obs_idx < x_length; ++obs_idx) {
      int obsv = X[seq_num*x_length + obs_idx];
      scale = 0.0;
      for (int state = 0; state < M; ++state) {

	// Sum on transitions from previous states to next state.
	float alph = 0.0;
	for (int state_prev = 0; state_prev < M; ++state_prev) {
	  alph += alpha(state_prev, obs_idx - 1) * A(state_prev, state);
	}

	// Weight by observation likelihood.
	alph = alph * B(state, obsv);

	// Set unscaled likelihood and update the scale.
	alpha(state, obs_idx) = alph;
	scale += alph;
      }
      
      // Add the scale to our scales.
      scales.push_back(scale);

      // Go back through and scale the likelihoods.
      for (int state = 0; state < M; ++state) {
	alpha(state, obs_idx) = alpha(state, obs_idx) / scales[obs_idx];
      }
    }

    // Now we have all our alphas and all our scales. Multiply scales to get total
    // probability of haven seen the given observations.
    float likelihood = 0.0;
    for (int obvs = 0; obvs < x_length; ++obvs) {
      likelihood += log(scales[obvs]);
    }

    probabilities.push_back(likelihood);
  }

  return probabilities;
}
