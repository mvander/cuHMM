/*
main.cpp

Run HMM code.
 */

#include "hmm.h"
#include <boost/algorithm/string.hpp>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <boost/numeric/ublas/matrix.hpp>
#include <ctime>
#include "timer.h"

int main() {
  hmm h1;

  // Setup file.
  std::string setup_file = "setup.txt";
  h1.set_model(setup_file);

  // Run forward algorithm.
  std::vector<std::string> observations_str;

  // Read in data from data file.
  std::string data_file = "data.txt";
  std::ifstream data;
  data.open(data_file);

  std::string line;
  if(!std::getline(data, line))
    return -1;

  boost::split(observations_str, line, boost::is_any_of(","));

  std::vector<int> observations;
  observations.resize(observations_str.size());

  for (int idx =0; idx < observations_str.size(); ++idx) {
    observations[idx] = std::stoi(observations_str[idx]);
  }

  int length = 9;

  double start, finish, elapsed;

  GET_TIME(start);

  std::vector<float> likelihoods = h1.forward(observations, length);

  GET_TIME(finish);

  elapsed = finish - start;
  std::cout << "Time was " << elapsed << std::endl;

  std::cout << "Likelihoods:" <<std::endl;
  for (int idx = 0; idx < likelihoods.size(); ++idx) {
    std::cout << likelihoods[idx] << std::endl;
  }
}
