/*
hmm.h

Define the HMM model functions.
 */

/**
 * HMM model definition.
 */
typedef struct {

  // Number of states.
  int M;

  // Size of vocabulary.
  int V;

  // Start distribution. 1xM
  float* pi;

  // Transition matrix. MxM
  float* A;

  //Emission matrix. MxV
  float* B;

} hmm;

/**
 * Create hmm based on the pi, A, and B definitions contained in the given
 * file.
 * 
 * @param filename - File w/ model parameters specified.
 * @return HMM struct pointer with pi, A, B specified.
 */
hmm* get_hmm_from_file(char const* filename);

/**
 * Read in hmm observations from file.
 *
 * @param filename - File w/ data.
 * @param num_seq - Pointer where to put the number of sequences.
 * @param x_length - Length of each sequence.
 * @return Location of data in memory.
 */
int* get_data_from_file(char const* filename, int* num_seq, int* x_length);

/**
 * Perform the forward algorithm (i.e., find the probability) on the provided matrix
 * of integer observations.
 *
 * Function prints out the probability of each provided sequence.
 *
 * @param h - HMM struct pointer that has pi, A, B setup.
 * @param X - observation matrix.
 * @param num_seq - Number of sequences in observation matrix.
 * @param x_length - Length of single sequence of observations. Should be same for all.
 */
void forward(hmm* h, int* X, int num_seq, int x_length);
