/*
main.cpp

Run HMM codes.
 */

#include "hmm.h"
#include "helper.h"
#include "timer.h"
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

int main() {
  bool repeat = false;
  for (int i = 10; i < 20; i = i + 10) {
    std::string setup = "m_tests/setup_" + std::to_string(i) + "_50.txt";

    char const* setup_filename = setup.c_str();

    std::string dataf= "m_tests/data_50_50_50.txt";
    char const* data_filename = dataf.c_str();

    // Read in and create an HMM struct.
    hmm* my_hmm;
    my_hmm = get_hmm_from_file(setup_filename);

    // Read in and create our data.
    int* X;
    int* num_seq;
    int* x_length;
    num_seq = (int*) malloc(sizeof(int));
    x_length = (int*) malloc(sizeof(int));

    X = get_data_from_file(data_filename, num_seq, x_length);

    //print_int_array(X, *num_seq, *x_length);

    //std::cout << "M: " << my_hmm->M << std::endl;
    //std::cout << "L: " << *x_length << std::endl;

    double start;
    GET_TIME(start);

    forward(my_hmm, X, *num_seq, *x_length);

    double end;
    GET_TIME(end);

    double runtime = end - start;

    //printf("Start: %e\n", start);
    //printf("End: %e\n", end);
    printf("%d, %e\n", i, runtime);

    // Free everything up.
    free(my_hmm->pi);
    free(my_hmm->A);
    free(my_hmm->B);
    free(my_hmm);

    if (i == 10 && !repeat) {
      i = 0;
      repeat = true;
    }
  }

  return 0;
}
