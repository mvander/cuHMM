/*
hmm.cu

Implementation of HMM model functions using CUDA.
 */

#include <stdio.h>
#include <math.h>
#include "block_reduce.cuh"
#include "hmm.h"
#include "helper.h"

hmm* get_hmm_from_file(char const* filename) {
  // Allocate our HMM object.
  hmm* my_hmm = (hmm*) malloc(sizeof(hmm));

  FILE* hmm_file;

  char* line_buf = NULL;
  size_t buf_length = 0;

  // Open the file.
  hmm_file = fopen(filename, "r");

  // TODO: error-handling.

  // Read in M/V.
  getline(&line_buf, &buf_length, hmm_file);
  char* M_char = strtok(line_buf, ",");
  int M = atoi(M_char);
  char* V_char = strtok(0, ",");
  int V = atoi(V_char);

  // Read in pi.
  float* pi = (float*) malloc(sizeof(float) * M);
  getline(&line_buf, &buf_length, hmm_file);
  char* trans_prob = strtok(line_buf, ",");
  for (int i = 1; i < M; ++i) {
    pi[i-1] = atof(trans_prob);
    trans_prob = strtok(0, ",");
  }
  pi[M-1] = atof(trans_prob);

  // Read in A.
  float* A = (float*) malloc(sizeof(float) * M * M);
  getline(&line_buf, &buf_length, hmm_file);
  trans_prob = strtok(line_buf, ",");
  for (int i = 1; i < (M * M); ++i) {
    A[i-1] = atof(trans_prob);
    trans_prob = strtok(0, ",");
  }
  A[(M*M)-1] = atof(trans_prob);

  // Read in B.
  float* B = (float*) malloc(sizeof(float) * M * V);
  getline(&line_buf, &buf_length, hmm_file);
  trans_prob = strtok(line_buf, ",");
  for (int i = 1; i < (M * V); ++i) {
    B[i-1] = atof(trans_prob);
    trans_prob = strtok(0, ",");
  }
  B[(M*V)-1] = atof(trans_prob);
  
  // Set the pi, A, B, M, and V fields of our model.
  my_hmm->M = M;
  my_hmm->V = V;
  my_hmm->pi = pi;
  my_hmm->A = A;
  my_hmm->B = B;

  return my_hmm;
}

int* get_data_from_file(char const* filename, int* num_seq, int* x_length) {
  FILE* data_file;

  data_file = fopen(filename, "r");
  
  char* line_buf = NULL;
  size_t buf_length = 0;

  // TODO: Error handling.

  // Read in num_seq and x_length.
  getline(&line_buf, &buf_length, data_file);
  char* num_seq_str = strtok(line_buf, ",");
  int n = atoi(num_seq_str);
  char* x_length_str = strtok(0, ",");
  int l = atoi(x_length_str);

  //printf("N is %d\n", n);
  //printf("X Length is %d\n", l);

  // Now read in the observations.
  int* X = (int*) malloc(n * l * sizeof(int));

  getline(&line_buf, &buf_length, data_file);
  char* obs_str = strtok(line_buf, ",");
  for (int i = 1; i < (n * l); ++i) {
    X[i-1] = atoi(obs_str);
    obs_str = strtok(0, ",");
  }
  X[(n*l)-1] = atoi(obs_str);

  //printf("Data is:\n");
  //print_int_array(X, n, l);

  *num_seq = n;
  *x_length = l;

  return X;
}

/************************
Forward Algorithm Kernels
************************/

__device__ int ceiling_division(int x, int y) {
  return x/y + (x%y != 0);
}

#define NUM_THREADS = 32;

__global__ void init_fwd(float* alpha, float* C, float* pi, float* B, int* X, int n, int m, int v, int l) {
  // Decide which chunk of sequences this block should handle.
  int seq_work = ceiling_division(n, 32);
  int sequence_start = blockIdx.x * seq_work;

  // Thread idx corresponds to the state M of m states this thread handles.
  int block_work = ceiling_division(m, 64);
  int state = threadIdx.x * block_work;
   
  for (int seq_num = sequence_start; seq_num < sequence_start + seq_work && seq_num < m; ++seq_num) {
    //printf("Working on sequence number %d\n", seq_num);
    // Get first observation.
    int x_0 = X[seq_num * l];

    float sum = 0.0;
    for (int s = state; s < state + block_work && s < m; ++s) {
      // Find index for this thread in alpha.
      int alpha_index = seq_num * m + s;
      //printf("Alpha index %d\n", alpha_index);

      // Calculate unscaled alpha prime.
      float unscaled = pi[state] * B[(state * v) + x_0];

      alpha[alpha_index] = unscaled;
      sum += unscaled;
    }

    __syncthreads();

    // Use CUB to perform a fast reduction on our alpha primes.
    __shared__ float c;
    typedef cub::BlockReduce<float, 1024> BlockReduceT;

    // Store alpha primes here for reduction.
    float data[1];
    data[0] = sum;

    // Allocate shared memory for BlockReduce.
    __shared__ typename BlockReduceT::TempStorage temp_storage;
    
    float aggregate = BlockReduceT(temp_storage).Sum(data);

    if (threadIdx.x == 0) {
      c = aggregate;
      // Write the scale sum to the c matrix.
      int c_index = seq_num * l;
      C[c_index] = c;
      //printf("C for %d is %f\n", seq_num,aggregate);
    }

    __syncthreads();
  
    // Now all threads have access to the sum via shared variable c.
    for (int s = state; s < state + block_work && s < m; ++s) {
      // Find index for this thread in alpha.
      int alpha_index = seq_num * m + s;

      // Calculate scaled alpha.
      alpha[alpha_index] = alpha[alpha_index] / c;
      //printf("Alpha: %f\n", alpha[alpha_index]);
      //printf("First alpha index for seq %d, state %d,: %d\n", seq_num, m, alpha_index);
    }
  }
}

__global__ void alpha_prime(float* alpha, int time, float* A, float* B, int n, int m) {
  // Determine range of alpha primes this block is responsible for.
  int work = ceiling_division((n*m), 64);
  int alpha_start = (blockIdx.x * work) + (n*m*time);
  
  int limit = n * m * (time+1);
  for (int alpha_idx = alpha_start; alpha_idx < alpha_start + work && alpha_idx < limit; ++alpha_idx) {
    // Determine which sequence alpha is part of.
    int sequence = (alpha_idx - (n * m * time)) / m;
    //printf("%d\n", sequence);
    int state = (alpha_idx - (n * m * time)) % m;
    //printf("State: %d\n", state);

    // Determine for the given alpha, what each thread should reduce on.
    int thread_work = ceiling_division(m, 32);
    int m_start = threadIdx.x * thread_work;
    
    float sum = 0.0;
    for (int previous_state = m_start; previous_state < m_start + thread_work && previous_state < m; ++previous_state) {
      int previous_alpha_index = ((time-1) * n * m) + (sequence * m) + previous_state;

      //printf("Prev. alpha index for seq %d, state %d,: %d\n", sequence, previous_state, previous_alpha_index);

      //printf("prev_alph: %f\n", alpha[previous_alpha_index]);
      //printf("A: %f\n", A[(previous_state*m) + state]);
      sum = sum + (alpha[previous_alpha_index] * A[(previous_state*m) + state]);
    }
    //printf("Sum: %f", sum);
  
    __syncthreads();
    typedef cub::BlockReduce<float, 32> BlockReduceT;

    // Alpha prime is summation of all previous states's alpha values times transition likelihood.
    __shared__ typename BlockReduceT::TempStorage temp_storage;
    float data[1];
    data[0] = sum;

    float aggregate = BlockReduceT(temp_storage).Sum(data);

    if (threadIdx.x == 0) {
      // Place unscaled alpha in its location.
      alpha[alpha_idx] = aggregate;
      //printf("Unscaled alpha: %f\n", aggregate);
    }
  }
}

// Similar approach to init_fwd.
__global__ void alpha_fwd(float* alpha, float* A, float* B, int* X, float* C, int l, int n, int m, int v, int time) {
  // Decide which chunk of sequences this block should handle.
  int seq_work = ceiling_division(n, 32);
  int sequence_start = blockIdx.x * seq_work;

  // Thread idx corresponds to the state M of m states this thread handles.
  int block_work = ceiling_division(m, 64);
  int state = threadIdx.x * block_work;
   
  for (int seq_num = sequence_start; seq_num < sequence_start + seq_work && seq_num < m; ++seq_num) {
    float sum = 0.0;
    for (int s = state; s < state + block_work && s < m; ++s) {
      // Find index for this thread in alpha.
      int alpha_index = (n * m * time) + (seq_num * m) + s;
      //printf("Alpha index %d\n", alpha_index);

      int x_t = X[(seq_num * l) + time]; // % v;
      // Calculate unscaled alpha prime.
      float unscaled = alpha[alpha_index] * B[(state * v) + x_t];
      alpha[alpha_index] = unscaled;
      sum += unscaled;
    }

    __syncthreads();

    // Use CUB to perform a fast reduction on our alpha primes.
    __shared__ float c;
    typedef cub::BlockReduce<float, 64> BlockReduceT;

    // Allocate shared memory for BlockReduce.
    __shared__ typename BlockReduceT::TempStorage temp_storage;
    float data[1];
    data[0] = sum;

    float aggregate = BlockReduceT(temp_storage).Sum(data);

    if (threadIdx.x == 0) {
      c = aggregate;
      // Write the scale sum to the c matrix.
      int c_index = seq_num * l + time;
      C[c_index] = c;
      //printf("Time: %d, seq %d: %f\n", time, seq_num, c);
    }

    __syncthreads();
  
    // Now all threads have access to the sum via shared variable c.
     for (int s = state; s < state + block_work && s < m; ++s) {
      // Calculate scaled alpha.
      int alpha_index = (n * m * time) + (seq_num * m) + s;
      alpha[alpha_index] = alpha[alpha_index] / c;
    }
  }
}

__global__ void sum_c(float* C, float* final_prob, int m, int n, int l) {
  //printf("%d\n", threadIdx.x);
  // n blocks w/ l threads (ideally)

  // Decide which chunk of sequences this block should handle.
  int seq_work = ceiling_division(n, 32);
  int sequence_start = blockIdx.x * seq_work;

  // Find time stamps this thread should handle.
  int time_work = ceiling_division(l, 32);
  int time_start = threadIdx.x * time_work;

  for (int seq_num = sequence_start; seq_num < sequence_start + seq_work && seq_num < m; ++seq_num) {
    
    float sum = 0.0;
    for (int time = time_start; time < time_start + time_work && time < l; ++time) {
      // Calculate the index in the C array this thread handles.
      int c_index = (seq_num * l) + time;
      
      float value = logf(C[c_index]);
      sum += value;
    }

    __syncthreads();
    typedef cub::BlockReduce<float, 32> BlockReduceT;

    // Sum all scale values for a given sequence to find probability of total sequence.
    __shared__ typename BlockReduceT::TempStorage temp_storage;
    float data[1];
    data[0] = sum;

    float aggregate = BlockReduceT(temp_storage).Sum(data);

    if (threadIdx.x == 0) {
      // Place final probability in location.
      final_prob[seq_num] = aggregate;
    }
  }
}

void forward(hmm* h, int* X, int num_seq, int x_length) {
  // Alpha is N x M x Length
  float* h_alpha = (float*) malloc(h->M * num_seq * x_length * sizeof(float));
  float* d_alpha;
  cudaMalloc((void**) &d_alpha, (h->M * num_seq * x_length * sizeof(float)));

  // C is N x Length
  float* h_C = (float*) malloc(num_seq * x_length * sizeof(float));
  float* d_C;
  cudaMalloc((void**) &d_C, (num_seq * x_length * sizeof(float)));

  // Final Probability is 1 x N.
  float* h_final_prob = (float*) malloc(num_seq * sizeof(float));
  float* d_final_prob;
  cudaMalloc((void**) &d_final_prob, (num_seq * sizeof(float)));

  // Make space on the GPU for pi, A, and B.
  float* d_pi;
  float* d_A;
  float* d_B;
  cudaMalloc((void**) &d_pi, (h->M * sizeof(float)));
  cudaMalloc((void**) &d_A, (h->M * h->M * sizeof(float)));
  cudaMalloc((void**) &d_B, (h->M * h->V * sizeof(float)));

  // Make space for our observations.
  int* d_X;
  cudaMalloc((void**) &d_X, (num_seq * x_length * sizeof(int)));

  //print_int_array(X, num_seq, x_length);

  // Copy over pi, A, B, X for upcoming kernels. Nothing for alpha and C.
  cudaMemcpy(d_pi, h->pi, (h->M * sizeof(float)), cudaMemcpyHostToDevice);
  cudaMemcpy(d_A, h->A, (h->M * h->M * sizeof(float)), cudaMemcpyHostToDevice);
  cudaMemcpy(d_B, h->B, (h->M * h->V * sizeof(float)), cudaMemcpyHostToDevice);
  cudaMemcpy(d_X, X, (num_seq * x_length * sizeof(int)), cudaMemcpyHostToDevice);

  // Kernel splits work N wise, amongst blocks. Each block uses it's threads for the reductions it must perform.
  init_fwd<<<32,64>>>(d_alpha, d_C, d_pi, d_B, d_X, num_seq, h->M, h->V, x_length);

  //printf("Initialization step done.\n");
  //printf("Length: %d\n", x_length);

  // Perform forward calculations for observations.
  for(int time = 1; time < x_length; ++time) {
    //printf("Calculating step %d.\n", time);
    alpha_prime<<<64, 32>>>(d_alpha, time, d_A, d_B, num_seq, h->M);
    alpha_fwd<<<32, 64>>>(d_alpha, d_A, d_B, d_X, d_C, x_length, num_seq, h->M, h->V, time);
  }

  //printf("Calculation complete.\n");
  sum_c<<<32,32>>>(d_C, d_final_prob, h->M, num_seq, x_length);

  // Copy alpha, c, and final probabilities back to see results.
  cudaMemcpy(h_alpha, d_alpha, (h->M * num_seq * x_length * sizeof(float)), cudaMemcpyDeviceToHost);
  cudaMemcpy(h_C, d_C, (num_seq * x_length * sizeof(float)), cudaMemcpyDeviceToHost);
  cudaMemcpy(h_final_prob, d_final_prob, (num_seq * sizeof(float)), cudaMemcpyDeviceToHost);

  printf("Final probabilities:\n");
  print_float_array(h_final_prob, 1, 1, num_seq);

  // Free up memory.
  cudaFree(d_alpha);
  cudaFree(d_C);
  cudaFree(d_pi);
  cudaFree(d_A);
  cudaFree(d_B);
  cudaFree(d_X);
  cudaFree(d_final_prob);
  free(h_alpha);
  free(h_C);
  free(h_final_prob);
}
