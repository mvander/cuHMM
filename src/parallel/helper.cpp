/*
helper.cpp

Define I/O helpers and other helpers as needed.
 */

#include <stdio.h>
#include "helper.h"

void print_float_array(float* array, int x, int y, int z) {
  for(int idx = 0; idx < (x*y*z); ++idx) {
    printf("%f, ", array[idx]);

    if ((idx + 1) % z == 0)
      printf("\n");
    if ((idx + 1) % (z * y) == 0)
      printf("\n");
  }
}

void print_int_array(int* array, int x, int y) {
  for (int idx = 0; idx < x; ++idx) {
    for (int idy = 0; idy < y; ++idy) {
      printf("%d, ", array[(idx*y) + idy]);
    }
    printf("\n");
  }
}
