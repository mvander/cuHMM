/*
helper.h

Declare I/O helpers and other helpers as needed.
 */

/**
 * Print out the given array of size x by y by z.
 *
 * @param array - pointer to front of array.
 */
void print_float_array(float* array, int x, int y, int z);

/**
 * Print out the given array of size x by y.
 *
 * @param array - pointer to front of array.
 * @param x - number of rows.
 * @param y - number of columns.
 */
void print_int_array(int* array, int x, int y);
