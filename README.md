### cuHMM: A CUDA Implementation of the Hidden Markov Model

This is an implementation of the Scaled Forward Algorithm used on Hidden Markov Models to determine the joint probability of an observation given a model. Created for CS4230 w/ Prof. Ganesh Gopalakrishnan in Spring 2018.


This work may be revisited in the future to extend and include other HMM algorithms including the Scaled Backward, Baum-Welch, and Viterbi algorithms.